#include "IO_Sensors_RKeso.h"
#include "../IRremote/IRremote.h"
#include "../IRremote/IRremoteInt.h"
#include "../NewTone/NewTone.h"

const int ledPlaca =13;

IO_Sensors_RKeso::IO_Sensors_RKeso(){
	pinMode(ledPlaca, OUTPUT);
	digitalWrite(ledPlaca, LOW);
}

int IO_Sensors_RKeso::SensorIluminacao(int pinoSinal, int limiteLuz){
	if(analogRead(pinoSinal) < limiteLuz){
		return 1;
	} else {
		return 0;
	}
}

void IO_Sensors_RKeso::LedPlaca(int atrasoOn, int atrasoOff){
	digitalWrite(ledPlaca, HIGH);
	delay(atrasoOn);
	digitalWrite(ledPlaca, LOW);
	delay(atrasoOff);
}

int IO_Sensors_RKeso::SensorMovimento(int pinoVCC, int pinoDATA){
	digitalWrite(pinoVCC, HIGH);
	return digitalRead(pinoDATA);
}

int IO_Sensors_RKeso::SensorDistancia(int ping, int echo, int distancia, int atraso){
	digitalWrite(ping, LOW);
  	delayMicroseconds(2);
  	digitalWrite(ping, HIGH);
  	delayMicroseconds(10);
  	digitalWrite(ping, LOW);
  	long duration = pulseIn(echo, HIGH);
  	long _distancia = duration /29 / 2 ;  

  	delay(atraso);
  	if(_distancia < distancia){
    		return 1;
  	} else{
    		return 0;
  	}
}

void IO_Sensors_RKeso::Rele(int pinoSinal, int estado){
	digitalWrite(pinoSinal, estado);//utiliza um transistor
}

int IO_Sensors_RKeso::BtnSensorReflexao(int pinoSinal){
	if(!digitalRead(pinoSinal)){
		return 1;
	} else {	
		return 0;
	}
}

void IO_Sensors_RKeso::Buzzer(int pinoSinal, int tom, int duracao, int qtdRepeticao){
	for(int cont = 0; cont < qtdRepeticao; cont ++){    
    		NewTone(pinoSinal, tom, duracao);
    		delay(duracao*10);
  	}
}

int IO_Sensors_RKeso::SensorIRRemote(decode_results results){
	
	float valorIR = (results.value); 
	
	if(valorIR == 0x61D6807F){
		return 1;
	} else if(valorIR == 0x61D640BF){
		return 2;
	} else if(valorIR == 0x61D6C03F){
		return 3;
	} else if(valorIR == 0x61D620DF){
		return 4;
	} else if(valorIR == 0x61D6A05F){
		return 5;
	} else if(valorIR == 0x61D6609F){
		return 6;
	} else if(valorIR == 0x61D6E01F){
		return 7;
	} else if(valorIR == 0x61D610EF){
		return 8;
	} else if(valorIR == 0x61D6906F){
		return 9;
	} else {
		return 0;
	}
}

void IO_Sensors_RKeso::CI_CD4511(int pinA, int pinB, int pinC, int pinD, int num, bool DTypeCatodo){

	int flag = DTypeCatodo;

	switch (num) {
		case 0:
			digitalWrite(pinA, !flag); digitalWrite(pinB, !flag); digitalWrite(pinC, !flag); digitalWrite(pinD, !flag);
			break;
		case 1:
			digitalWrite(pinA, flag); digitalWrite(pinB, !flag); digitalWrite(pinC, !flag); digitalWrite(pinD, !flag);
			break;
		case 2:
			digitalWrite(pinA, !flag); digitalWrite(pinB, flag); digitalWrite(pinC, !flag); digitalWrite(pinD, !flag);
			break;
		case 3:
			digitalWrite(pinA, flag); digitalWrite(pinB, flag); digitalWrite(pinC, !flag); digitalWrite(pinD, !flag);
			break;
		case 4:
			digitalWrite(pinA, !flag); digitalWrite(pinB, !flag); digitalWrite(pinC, flag); digitalWrite(pinD, !flag);
			break;
		case 5:
			digitalWrite(pinA, flag); digitalWrite(pinB, !flag); digitalWrite(pinC, flag); digitalWrite(pinD, !flag);
			break;
		case 6:
			digitalWrite(pinA, !flag); digitalWrite(pinB, flag); digitalWrite(pinC, flag); digitalWrite(pinD, !flag);
			break;
		case 7:
			digitalWrite(pinA, flag); digitalWrite(pinB, flag); digitalWrite(pinC, flag); digitalWrite(pinD, !flag);
			break;
		case 8:
			digitalWrite(pinA, !flag); digitalWrite(pinB, !flag); digitalWrite(pinC, !flag); digitalWrite(pinD, flag);
			break;
		case 9:
			digitalWrite(pinA, flag); digitalWrite(pinB, !flag); digitalWrite(pinC, !flag); digitalWrite(pinD, flag);
			break;
		default:
			digitalWrite(pinA, flag); digitalWrite(pinB, flag); digitalWrite(pinC, flag); digitalWrite(pinD, flag);

	}
}

void IO_Sensors_RKeso::CI_47HC595(int pinA, int pinB, int pinC, int num, bool DTypeCatodo){

	byte data;
	byte dataArray[17];
  
	dataArray[0] = 0x03; // 0 - 0b00000011
	dataArray[1] = 0x9F; // 1 - 0b10011111
	dataArray[2] = 0x25; // 2 - 0b00100101
	dataArray[3] = 0x0D; // 3 - 0b00001101
	dataArray[4] = 0x99; // 4 - 0b10011001
	dataArray[5] = 0x49; // 5 - 0b01001001
	dataArray[6] = 0x41; // 6 - 0b01000001
	dataArray[7] = 0x1F; // 7 - 0b00011111
	dataArray[8] = 0x01; // 8 - 0b00000001
	dataArray[9] = 0x19; // 9 - 0b00011001
	dataArray[10] = 0x11; // A - 0b00010001
	dataArray[11] = 0xC1; // B - 0b11000001
	dataArray[12] = 0x63; // C - 0b01100011
	dataArray[13] = 0x85; // D - 0b10000101
	dataArray[14] = 0x61; // E - 0b01100001
	dataArray[15] = 0x71; // F - 0b01110001
	dataArray[16] = 0xFE; // . - 0b11111110
 
	data = dataArray[num];
	digitalWrite(pinA, LOW);

	int flag = DTypeCatodo;

	for (int k=0; k <= 7; k++) {    
		digitalWrite(pinB, LOW);       
		if ( data & (1 << k) ) {
			digitalWrite(pinC, !flag);
		} else {
			digitalWrite(pinC, flag);
		}
		digitalWrite(pinB, HIGH);
	}    
	digitalWrite(pinB, LOW); 
	digitalWrite(pinA, HIGH);
}