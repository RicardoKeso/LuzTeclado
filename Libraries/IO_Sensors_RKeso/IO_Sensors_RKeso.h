#ifndef IO_Sensors_RKeso_H
    #define IO_Sensors_RKeso_H
     
    #include <Arduino.h>
    #include "../IRremote/IRremote.h"
    #include "../IRremote/IRremoteInt.h"
    #include "../NewTone/NewTone.h"
     
    class  IO_Sensors_RKeso {
        public:
            IO_Sensors_RKeso();
            int SensorIluminacao(int pinoSinal, int limiteLuz);
            void LedPlaca(int atrasoOn, int atrasoOff);
            int SensorMovimento(int pinoVCC, int pinoDATA);
            int SensorDistancia(int ping, int echo, int distancia, int atraso);
            void Rele(int pinoSinal, int estado);
            int BtnSensorReflexao(int pinoSinal);
            void Buzzer(int pinoSinal, int tom, int duracao, int qtdRepeticao);
            int SensorIRRemote(decode_results results);
	        void CI_CD4511(int pinA, int pinB, int pinC, int pinD, int num, bool DTypeCatodo);
	        void CI_47HC595(int pinA, int pinB, int pinC, int num, bool DTypeCatodo);
    };
 
#endif